<!DOCTYPE html>
<html>
<head>
    <title>Doa Pet - O lugar para você adotar ou doar um pet.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--    Stilos-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<!--    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">-->

    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/">

    <!--    Scripts-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

</head>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="index.php">
        <img src="img/petlogom.png" class="h_logo" alt="Blogin" title="">
    </a>
    <li class="titulo_nav_bar" ">
        <span class="row">Doa Pet - O lugar para você adotar ou doar um pet</span>
    </li>
    <ul class="navbar-nav" style="margin-left: 80%; position: absolute">
    <!-- Links -->
        <li><a id="links" class="dropdown-item" href="index.php?page=page_adotar">Adoção</a></li>
        <li><a id="links" class="dropdown-item" href="index.php?page=page_doacao">Doação</a></li>
    </ul>
</nav>

    <body>

    <div class="container">
        <div class="row">
            <div class="col-lg-12" id="mainPage">
                <?php
                include("conf.php");
                switch(@$_REQUEST["page"]){
                    case "page_doacao":
                        include("cadastro_usuario.php");
                        break;
                        case "page_adotar":
                        include("adotar.php");
                        break;
                    case "salvar_usuario":
                        include("salvar-usuario.php");
                        break;
                    case "e_usuario":
                        include("editar-usuario.php");
                        break;
                    case "listar_usuario":
                        include("listar-usuario.php");
                        break;
                    case "detalhes_adocao":
                        include("info_adocao.php");
                        break;
                    case "salvar_adocao":
                        include("salvar-adocao.php");
                        break;
                    default:
                        include("adotar.php");
                }
                ?>
            </div>
        </div>
    </div>

    <section id="footer" class="footer">
        <div class="container">
            <div class="row text-center text-xs-center text-sm-left text-md-left">
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <h5>Páginas</h5>
                    <ul class="list-unstyled quick-links">
                        <li><a href="index.php?page=page_doacao"><i class="fa fa-angle-double-right"></i>Doação</a></li>
                        <li><a href="index.php?page=page_adotar"><i class="fa fa-angle-double-right"></i>Adoção</a></li>
                    </ul>
                </div>
                <div class="social_media">
                    <ul class="list-unstyled list-inline social text-center">
                        <li class="list-inline-item"><a href="https://www.facebook.com/artur.silva.33671/"><i class="fa fa-facebook" target="_blank"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-twitter"></i></a></li>
                    </ul>

                    <div id="direitosDecopia">
                        <p><u><a href="#">Projeto Integrador II</a></u></p>
                        <p class="h6">&copy All right Reversed.<a class="text-green ml-2" href="#" target="_blank">Doa Pet</a></p>
                    </div>
                    </hr>
                </div>
                </hr>

            </div>
        </div>
    </section>
    </body>
</html>