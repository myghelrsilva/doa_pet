<h1>Lista de usuários</h1>
<?php
$sql = "SELECT * FROM usuarios";

$result = $conn->query($sql) or die($conn->error);

$qtd = $result->num_rows;

if($qtd > 0){
    print "$qtd</b> registros(s) encontratod(s)</p>";
    print "<table class='table table-bordered table-striped table-hover'>";
    print "<tr>";
    print "<th>#</th>";
    print "<th>Nome</th>";
    print "<th>Email</th>";
    print "<th>Ações</th>";
    print "</tr>";
    while( $row = $result->fetch_assoc() ){
        print "<tr>";
        print "<td>".$row["id"]."</td>";
        print "<td>".$row["nome"]."</td>";
        print "<td>".$row["email"]."</td>";
        print "<td>
			
				   <button class='btn btn-success' onclick=\"location.href='index.php?page=e_usuario&id=".$row["id"]."'\"><i class=\"material-icons\">edit</i></button>
				   
				   <button class='btn btn-danger' onclick=\"location.href='index.php?page=salvar_usuario&acao=excluir&id=".$row["id"]."'\"><i class=\"material-icons\">delete</i></button>
				   
				   </td>";
        print "</tr>";
    }
    print "</table>";
}else{
    print "Nnehum registro encontrado";
}
?>


