<section>
    <h3>Dados Pessoais</h3>
    <form action="index.php?page=salvar_usuario&acao=cadastrar" method="POST" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label>Nome Completo</label>
                <input type="text" name="nome" class="form-control">
            </div>
            <div class="form-group col-md-4">
                <label>Email</label>
                <input type="text" name="email" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label>Telefone</label>
                <input type="tel" name="telefone" class="form-control">
            </div>
        </div>
        <hr>
        <h3>Endereço</h3>
        <div class="form-group">
            <label for="inputAddress">Endereço</label>
            <input type="text" name="complemento" class="form-control" id="inputAddress" placeholder="Rua dos Bobos, nº 0">
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputCity">Cidade</label>
                <input type="text" name="cidade" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEstado">Estado</label>
                <select name="estado" id="inputEstado" class="form-control">
                    <option selected>Escolher...</option>
                    <option>DF</option>
                    <option>GO</option>
                    <option>MG</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <hr>
            <h3>Informaçõe adicionais</h3>
            <label>Descricao</label>
            <textarea name="descricao" class="form-control" maxlength="100"  rows="3"></textarea>
        </div>
            Arquivo: <input type="file" required name="arquivo">
        <div class="form-group">
            <label>Anúncio</label>
            <textarea name="anuncio" class="form-control" maxlength="200" rows="4"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</section>